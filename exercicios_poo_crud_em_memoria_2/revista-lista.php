<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Lista de Revistas</title>
	</head>
	<body>
			
			<?php 
				require_once("colecaoderevistasemmemoria.php");
				require_once("revista.php");
				require_once("editora.php");
				
				if(isset($_POST['id']) and isset($_POST['nome']) and isset($_POST['mes']) and isset($_POST['ano']) and isset($_POST['editora'])) {
				if(isset($_POST['nome'])) {
					$revista = new Revista($_POST['id'], $_POST['nome'], $_POST['mes'], $_POST['ano'], $_POST['editora']);
					echo $_POST['nome'];
				}		
				
				$editora1 = new Editora(5, "Erika");
				
				$revista1 = new Revista(1, "Men's Health", 10, 2014, $editora1);
				$revista2 = new Revista(2, "Veja", 8, 2012, $editora1);
				$revista3 = new Revista(3, "�poca", 3, 2010, $editora1);
				$revista4 = new Revista(4, "History", 9, 2011, $editora1);
				
				$revistasEmMemoria = new ColecaoDeRevistasEmMemoria();
				$revistasEmMemoria->adicionar($revista1);
				$revistasEmMemoria->adicionar($revista2);
				$revistasEmMemoria->adicionar($revista3);
				$revistasEmMemoria->adicionar($revista4);
				$revistas = $revistasEmMemoria->todos();
				
				echo "<table border='1'>";
				
				echo "<tr>";
				echo "<td>ID</td>";
				echo "<td>Nome</td>";
				echo "<td>M�s</td>";
				echo "<td>Ano</td>";
				echo "<td>Revista</td>";
				echo "<td></td>";
				echo "</tr>";
				
				foreach ($revistas as $r) {
					echo "<tr>";
					echo "<td>" . $r->id() . "</td>";
					echo "<td>" . $r->nome() . "</td>";
					echo "<td>" . $r->mes() . "</td>";
					echo "<td>" . $r->ano() . "</td>";
					echo "<td>" . $r->editora()->nome() . "</td>";
					echo "<td>" . "<a href='revista-form.php?id=" . $r->id() . "' ><img src='Pencil-icon.png'></a>" . "<a href='revista-remover.php?id=" . $r->id() . "' ><img src='Actions-edit-delete-icon.png'></a>" . "</td>";
					echo "</tr>";
				}
	
				echo "</table>";
				
			?>
		
			<form id='f' name='f' action='revista-form.php' method='get'>
				<input type='submit' id='cadastrar' name='cadastrar' value='Cadastrar' />
			</form>
	</body>
</html>