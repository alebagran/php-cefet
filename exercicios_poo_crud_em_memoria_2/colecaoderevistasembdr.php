<?php 

class ColecaoDeRevistasEmBDR implements ColecaoDeRevistas {
	// Faz c q a doc da classe pai seja herdada
	/**
	 * @inheritDoc 
	 */
	 
	// aproveitar o da classe colecaodeditorasembdr
	function __construct(PDO $pdo, ColecaoDeEditoras $editoras) {
		$this->pdo = $pdo;
		$this->editoras = $editoras;
	}
	
	function executar() { 
			
	}
	
	function adicionar( &$item ) { // Passagem de par�metro por refer�ncia
		try {
			$sql = 'INSERT INTO revista ( nome, mes, ano, editora_id ) VALUES( :nome, :mes, :ano, :editora_id )';
			$this->executar( $sql, array(
			'nome' => $item->getNome(),
			'ano' => $item->getAno(),
			'mes' => $item->getEditora()->getId()			
			) );
			// Coloca o ID gerado pelo banco de dados na Revista
			$item->setId( $pdoi->lastInsertId() );
		} catch (PDOException $e) {
			throw new ColecaoException( 'Erro ao adicionar a revista', 0, $e );
		}
	}
	
	function removerPeloiD( $id ) {
		$sql = 'DELETE FROM revista WHERE id = :id';
		try {
			$this->executar( $sql, array( 'id' => $id ) );
		} catch (PDOException $e) {
			throw new ColecaoException('Erro ao remover a revista', 0, $e);
		}
	}
	
	function comId($id) {
		$sql = 'SELECT * FROM revista';
		$ps = $this->executar( $sql );
		$revistas = array();
		foreach($ps as $registro) {
			// Pega o id da editora que est� na coluna editora "editora_id"
			// do registro da revista e usa-o para carregar o "objeto"
			// da editora
			$editora = $this->editoras->comId( $registro )[ 'editora_id' ];
				
				
			$revista = new Revista($registro['id'],
					$registro['nome'],
					$registro['mes'],
					$registro['ano'],
					$editora // Objeto da classe editora
			);
				
				
			// Adiciona a revista no array
			$revistas[] = $revista; // array_push($revistas, $revista);
			
			if( count($revistas < 1))
				return 0;
			return $revista; 
	}
	
	function revistas($sql, array $parametros() ) {
		
	}
	
	function comId() {
		
		
	}
	
	function todos() {
		$sql = 'SELECT * FROM revista';
		$ps = $this->executar( $sql );
		$revistas = array();
		foreach($ps as $registro) {
			// Pega o id da editora que est� na coluna editora "editora_id"
			// do registro da revista e usa-o para carregar o "objeto"
			// da editora
			$editora = $this->editoras->comId( $registro )[ 'editora_id' ]; 
			
			
			$revista = new Revista($registro['id'],
					$registro['nome'],
					$registro['mes'],
					$registro['ano'],
					$editora // Objeto da classe editora
					);
			
			
			// Adiciona a revista no array
			$revistas[] = $revista; // array_push($revistas, $revista);
		}
	}
	
	function atualizar( $obj ) {
		try {
			$this->executar( $sql, array('UPDATE revista SET' .
										'nome = :nome,' .
										'ano = :ano,' .
										'mes = :mes' .
										'WHERE id = :editora_id' .
										'WHERE id = :id') );
			
		} catch (PDOException $e) {
			throw new ColecaoException('Erro ao remover a revista', 0, $e);
		}
		
		// Para o todos:
		
		$ps = "SELECT COUNT( id ) AS \'CONTAGEM'\ FROM revista";
		try {
			$ps = $this->executar( $sql );
			$resultado = $ps->fetchAll();
			return $resultado[0]['COUNT'];
		} catch (PDOException $e) {
			throw new ColecaoException('Erro ao realizar a contagem', 0, $e);
		}
	}	
	
	}

?>