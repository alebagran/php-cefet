<?php
	class Revista {
		private $id;
		private $nome;
		private $mes;
		private $ano;
		/** @var Editora */
		private $editora;
		
		function __construct($id, $nome, $mes, $ano, Editora $editora) { // Ao passar o tipo da editora, vc for�a 2 coisas: evita passar null e obriga a passar tipo editora 
			$this->id = $id;
			$this->nome = $nome;
			$this->mes = $mes;
			$this->ano = $ano;
			$this->editora = $editora;
		}
		
		function setId($id) {
			$this->id = $id;
		}
		
		function setNome($nome) {
			$this->nome = $nome;
		}
		
		function setMes($mes) {
			$this->mes = $mes;
		}
		
		function setAno($ano) {
			$this->ano = $ano;
		}
		
		function setEditora($editora) {
			$this->editora = $editora;
		}
		
		function id() {
			return $this->id;
		}
		
		function nome() {
			return $this->nome;
		}
		
		function mes() {
			return $this->mes;
		}
		
		function ano() {
			return $this->ano;
		}
		
		function editora() {
			return $this->editora;
		}
	}
?>