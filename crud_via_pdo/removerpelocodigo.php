
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Remover Produto Pelo Codigo</title>
	</head>
	<body>
			<h1>Insira o codigo do produto que deseja remover</h1>
			
			<?php 
				header('Content-Type: text/html; charset=UTF-8');
				if(isset($_GET['codigo'])) {
					$pdo = null;
					try {
						$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
						$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					} catch (PDOException $e) {
						echo 'Falha ao conectar: ' . $e->getMessage();
					}
					
					$ps = $pdo->prepare('DELETE FROM produto WHERE codigo=?');
					try {
						$ps->execute(array($_GET['codigo']));
					} catch (PDOException $e) {
						echo 'Erro ao remover o item: ' . $e->getMessage();
					}
					
					header('Location: exercicio4.php');
				}
			?>
		
			<form id='f' name='f' action='<?php echo $_SERVER['PHP_SELF'] ?>' method='get'>
				<label for='preco' >Quantidade Maxima:</label>
				<input type='text' id='codigo' name='codigo' /><br />
				<input type='submit' id='remover' name='remover' value='Remover' />
			</form>
		
	</body>
</html>