<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Produtos em Ordem Alfabetica</title>
	</head>
	<body>
			<h1>Produtos em Ordem Alfabetica</h1>
			
			<?php 
			header('Content-Type: text/html; charset=UTF-8');
				$pdo = null;
				try {
					$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
					$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
					$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				} catch(PDOException $e) {
					echo 'Falha ao conectar: ' . $e->getMessage();
				}
				
				$ps = $pdo->prepare('SELECT * FROM produto ORDER BY nome ASC');
				try {
					$ps->execute(array());
				} catch (PDOException $e) {
					echo 'Erro ao ordenar os produtos: ' . $e->getMessage();
				}
				
				echo '<table border="1">' .
				"<tr>" .
				"<th>Codigo:</th>" .
				"<th>Nome:</th>" .
				"<th>Quantidade:</th>" .
				"<th>Preco:</th>" .
				"</tr>";
				
				foreach($ps as $p) {
						echo '<tr>' .
						"<td>" . $p['codigo'] . "</td>" .
						"<td>" . $p['nome'] . "</td>" .
						"<td>" . $p['quantidade'] . "</td>" .
						"<td>" . $p['preco'] . "</td>" .
						"<tr />";
				}
				echo '</table>';
				
				echo '<br />';
			
			?>
		
			<form id='f' name='f' action='exercicio4.php' method='get'>
				<input type='submit' id='voltar' name='voltar' value='Voltar' />
			</form>
		
	</body>
</html>