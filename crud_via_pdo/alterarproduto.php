<?php
	header('Content-Type: text/html; charset=UTF-8');
	$pdo = null;
	try {
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		echo 'Falha ao conectar: ' . $e->getMessage();
	}
	
	
	$ps = $pdo->prepare('UPDATE produto SET nome=?, quantidade=?, preco=? WHERE id=?');
	try {
		$ps->execute(array($_POST['nome'], $_POST['quantidade'], $_POST['preco'], $_POST['id']));
	} catch (PDOException $e) {
		echo 'Erro ao remover o item: ' . $e->getMessage();
	}
	
	header('Location: exercicio4.php');

?>