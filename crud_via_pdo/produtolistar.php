<?php
	header('Content-Type: text/html; charset=UTF-8');
	$pdo = null;
	try {
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$pdo = new PDO('mysql:dbname=empresa;host=localhost', 'root', 'jony1723', $options);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		die('Falha ao conectar: ' . $e->getMessage());
	}
	
	echo 'Conectado <br/><br/>';
	
	$ps = $pdo->prepare('SELECT * FROM produto');
	try {
		$ps->execute(array());
	} catch(PDOException $e) {
		die('Erro executando consulta: ' . $e->getMessage());
	}
	
	/*
	foreach($ps as $p) {
		echo 'Nome: ' . $p['nome'] . '<br />';
		echo 'Preço: ' . $p['preco'] . '<br />';
	}
	*/
	
	while ($p = $ps->fetchObject()) {
		echo 'Nome: ' . $p->nome . '<br />';
		echo 'Preço: ' . $p->preco . '<br />';
	}
	
	
?>