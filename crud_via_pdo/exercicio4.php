<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
			<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Pesquisa de contato por nome ou telefone</title>
	</head>
	<body>
			
			<?php 
			header('Content-Type: text/html; charset=UTF-8');
			$pdo = null;
			try {
				$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
				$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				echo 'Falha ao conectar: ' . $e->getMessage();
			}
			
			$ps = null;
			$ps = $pdo->prepare('SELECT * FROM produto');
			try {
				$ps->execute(); // Para consulta posso usar query. Para mexer com dados, devo usar prepare e execute.
			} catch(PDOException $e) {
				die('Erro executando consulta: ' . $e->getMessage());
			}
			
			echo '<table border="1">' .
				'<tr>' .
				'<th>Codigo:</th>' .
				'<th>Nome:</th>' .
				'<th>Quantidade:</th>' .
				'<th>Preco:</th>' .
				'<th>Remover:</th>' .
				'<th>Alterar:</th>' .
				'</tr>';
			
			foreach($ps as $p) {
				echo '<tr>' .
					"<td>" . $p['codigo'] . '</td>' .
					"<td>" . $p['nome'] . '</td>' .
					"<td>" . $p['quantidade'] . '</td>' .
					"<td>" . $p['preco'] . '</td>' .
					"<td><a href='removerproduto.php?id=" . $p['id'] . "'>Remover</a></td>" .
					"<td><a href='insereproduto.php?id=" . $p['id'] . "'>Alterar</a></td>" .
					"<tr />";
			}
			echo '</table>';
			
			echo '<br />';
			?>
			
		<form id='f' name='f' action='insereproduto.php' method='get'>
			<input type='submit' id='adicionar' name='adicionar' value='Adicionar' />
		</form>
		
		<form id='f' name='f' action='consultaordemalfabetica.php' method='get'>
			<input type='submit' id='consultaordemalfabetica' name='consultaordemalfabetica' value='Ordenar por Nome' />
		</form>
			
		<form id='f' name='f' action='consultaprecomaximo.php' method='get'>
			<input type='submit' id='consultaprecomaximo' name='consultaprecomaximo' value='Consulta por Preco Maximo' />
		</form>
		
		<form id='f' name='f' action='consultaquantidade.php' method='get'>
			<input type='submit' id='consultaquantidade' name='consultaquantidade' value='Consulta por Quantidade' />
		</form>
		
		<form id='f' name='f' action='aumentarpercentualdopreco.php' method='get'>
			<input type='submit' id='aumentarpercentualdopreco' name='aumentarpercentualdopreco' value='Aumentar Percentual do Preço' />
		</form>
		
		<form id='f' name='f' action='diminuirporpreco.php' method='get'>
			<input type='submit' id='diminuirporpreco' name='diminuirporpreco' value='Diminuir Quantidade por Preço' />
		</form>
		
		<form id='f' name='f' action='removerpelocodigo.php' method='get'>
			<input type='submit' id='removerpelocodigo' name='removerpelocodigo' value='Remover pelo Codigo' />
		</form>
		
		</body>
</html>