<?php
header('Content-Type: text/html; charset=UTF-8');
$pdo = null;
try {
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
	$pdo = new PDO('mysql:dbname=empresa;host=localhost', 'root', 'jony1723', $options);
} catch(PDOException $e) {
	echo "Falha ao conetcar: " . $e->getMessage();
}

$ps = $pdo->prepare("SELECT * FROM contato WHERE nome LIKE ?");
$query = '%';
if(! $ps->execute(array(('Jo' . $query))))
	die('Erro na execução do comando.');
if($ps->rowCount() < 1)
	die('Nenhum registro encontrado.');

echo "<br />Nome iniciados com 'Jo':";

foreach($ps as $p) {
	echo '<br />';
	echo "Id: " . $p['id'] . '<br />';
	echo "Nome: " . $p['nome'] . '<br />';
	echo "Telefone: " . $p['telefone'] . '<br />';
	echo '---------------------------<br />';
}

$ps = $pdo->prepare("SELECT * FROM contato WHERE telefone LIKE ?");
$query = '%';
if(! $ps->execute(array('228' . $query)))
	die('Erro na execução do comando.');
if($ps->rowCount() < 1)
	die('Nenhum registro encontrado.');

echo "<br />Telefones iniciados com '228':";

foreach($ps as $p) {
	echo '<br />';
	echo "Id: " . $p['id'] . '<br />';
	echo "Nome: " . $p['nome'] . '<br />';
	echo "Telefone: " . $p['telefone'] . '<br />';
	echo '---------------------------<br />';

}


?>