<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Consulta por Preço</title>
	</head>
	<body>
			<h1>Produtos</h1>
		
			<?php
			header('Content-Type: text/html; charset=UTF-8');
			if(isset($_GET['precomaximo'])) {
				$pdo = null;
				try {
					$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
					$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
				} catch(PDOException $e) {
					echo 'Falha ao conectar: ' . $e->getMessage();
				}
					
				$ps = $pdo->prepare('SELECT * FROM produto WHERE preco<=? ORDER BY preco DESC');
				$ok = $ps->execute(array($_GET['precomaximo']));
				if(!$ok) {
					echo "<form id='f' name='f' action='exercicio4.php' method='get'>
								<input type='submit' id='voltar' name='voltar' value='Voltar' />
							</form>";
					die("Erro executando o comando.");
				}
					
				if($ps->rowCount() < 1) {
					echo "<form id='f' name='f' action='exercicio4.php' method='get'>
								<input type='submit' id='voltar' name='voltar' value='Voltar' />
							</form>";
					die("Nenhum registro encontrado.");
				}
				else {
					echo '<table border="1">';
					echo '<tr>';
					echo '<th>Codigo:</th>';
					echo '<th>Nome:</th>';
					echo '<th>Quantidade:</th>';
					echo '<th>Preco:</th>';
					echo '</tr>';
						
					foreach($ps as $p) {
						echo '<tr>';
						echo "<td>" . $p['codigo'] . '</td>';
						echo "<td>" . $p['nome'] . '</td>';
						echo "<td>" . $p['quantidade'] . '</td>';
						echo "<td>" . $p['preco'] . '</td>';
						echo '<tr />';
					}
					echo '</table>';
				}
			}

			?>
			
			<form id='f' name='f' action='exercicio4.php' method='get'>
				<input type='submit' id='voltar' name='voltar' value='Voltar' />
			</form>
	</body>
</html>
