<?php
header('Content-Type: text/html; charset=UTF-8');
foreach(PDO::getAvailableDrivers() as $driver) {
	echo $driver, '<br />';
}

$pdo = null;
try {
	$dsn = 'mysql:dbname=empresa;host=localhost';
	$username = 'root';
	$passwd = 'jony1723';
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
	$pdo = new PDO($dsn, $username, $passwd, $options);
} catch(PDOException $e) {
	die('Falha ao conectar: ' . $e->getMessage());
}

/*
$ps = $pdo->query('select * from contato');
$contatos = $ps->fetchAll();
echo '<br /><br />';
foreach ($contatos as $c) {
	echo 'C�digo: ', $c['id'], '<br />';
	echo 'Nome: ', $c['nome'], '<br />';
	echo 'Telefone: ', $c['telefone'], '<br />';
	echo '--------------------------<br />';
}
*/

// Se for usar o foreach, � melhor usar diretamente o PDOStatement em vez de usar o fetchAll() (acima)

$ps = $pdo->query('select * from contato');
foreach ($ps as $p) {
	echo 'Código: ', $p['id'], '<br />';
	echo 'Nome: ', $p['nome'], '<br />';
	echo 'Telefone: ', $p['telefone'], '<br />';
	echo '--------------------------<br />';
}

echo '<br />';

$ps = $pdo->query('select * from contato');

while($p = $ps->fetchObject()) {
	echo 'Código: ', $p->id, '<br />';
	echo 'Nome: ', $p->nome, '<br />';
	echo 'Telefone: ', $p->telefone, '<br />';
	echo '--------------------------<br />';
}



?>