<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
			<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Tabela de Contas</title>
	</head>
	<body>
			
			<?php 
			header('Content-Type: text/html; charset=UTF-8');
			
			$pdo = null;
			try {
				$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
				$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				echo 'Falha ao conectar: ' . $e->getMessage();
			}
			
			if(isset($_POST['numerocontaorigem']) and isset($_POST['numerocontadestino']) and isset($_POST['valordetransferencia'])) {	
				try {
					$pdo->beginTransaction();
			
					$ps = $pdo->prepare('SELECT * FROM conta_bancaria WHERE numero = :numero');
					$ps->execute(array('numero' => $_POST['numerocontaorigem']));
					if($ps->rowCount() < 1) {
						throw new PDOException('O número informado para a conta origem não existe.');
					}
			
					if($ps->fetchColumn(2) < $_POST['valordetransferencia']) {
						throw new PDOException('O saldo da conta de origem é insuficiente');
					}
			
					$ps = $pdo->prepare('SELECT * FROM conta_bancaria WHERE numero = :numero');
					$ok2 = $ps->execute(array('numero' => $_POST['numerocontadestino']));
					if($ps->rowCount() < 1) {
						throw new PDOException('O número informado para a conta destino não existe.');
					}
			
					$ps = $pdo->prepare('UPDATE conta_bancaria SET saldo = saldo - :saldo WHERE numero = :numero');
					$ps->execute(array('saldo' => $_POST['valordetransferencia'], 'numero' => $_POST['numerocontaorigem']));
			
			
					$ps = $pdo->prepare('UPDATE conta_bancaria SET saldo = saldo + :saldo WHERE numero = :numero');
					$ok2 = $ps->execute(array('saldo' => $_POST['valordetransferencia'], 'numero' => $_POST['numerocontadestino']));
			
					$pdo->commit();
			
				} catch (PDOException $e) {
					$pdo->rollBack();
					echo 'Erro ao realizar a operação => ' . $e->getMessage();
				}
			}		
			
			$ps = null;
			try {
				$ps = $pdo->query('SELECT * FROM conta_bancaria'); // Para consulta posso usar query. Para mexer com dados, devo usar prepare e execute.
			} catch(PDOException $e) {
				die('Erro executando consulta: ' . $e->getMessage());
			}
			
			echo '<table border="1">' .
				'<tr>' .
				'<th>Numero:</th>' .
				'<th>Saldo:</th>' .
				'<th>Remover:</th>' .
				'<th>Alterar:</th>' .
				'</tr>';
			
			foreach($ps as $p) {
				echo '<tr>' .
					"<td>" . $p['numero'] . '</td>' .
					"<td>" . $p['saldo'] . '</td>' .
					"<td><a href='removerconta.php?id=" . $p['id'] . "'>Remover</a></td>" .
					"<td><a href='insereconta.php?id=" . $p['id'] . "'>Alterar</a></td>" .
					"<tr />";
			}
			echo '</table>';
			
			echo '<br />';
			?>
			
		<form id='f' name='f' action='insereconta.php' method='get'>
			<input type='submit' id='adicionar' name='adicionar' value='Adicionar' />
		</form>
		
		<form id='f' name='f' action='transferenciabancaria.php' method='get'>
			<input type='submit' id='transferencia' name='transferencia' value='Realizar Transferencia' />
		</form>
		
		<h1>Transferência Bancária</h1>
		
		<h3>Insira os valores para realizar a sua transferencia bancária: </h3>
		
		<form id='f' name='f' action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post'>
			<label for='numerocontaorigem' >Numero da Conta de Origem:</label>
			<input type='text' id='numerocontaorigem' name='numerocontaorigem' /><br />
			<label for='numerocontadestino' >Numero da Conta Destino:</label>
			<input type='text' id='numerocontadestino' name='numerocontadestino' /><br />
			<label for='valordetransferencia' >Valor:</label>
			<input type='text' id='valordetransferencia' name='valordetransferencia' /><br />
			<input type='submit' id='transferir' name='transferir' value='Transferir' />
		</form>
		</body>
</html>