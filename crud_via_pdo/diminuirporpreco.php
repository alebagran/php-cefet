
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Consulta por Quantidade</title>
	</head>
	<body>
	
			<?php 
				header('Content-Type: text/html; charset=UTF-8');
				if(isset($_POST['preco']) and isset($_POST['quantidade'])) {
					$pdo = null;
					try {
						$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
						$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					} catch (PDOException $e) {
						echo 'Falha ao conectar: ' . $e->getMessage();
					}
					
					$ps = $pdo->prepare('UPDATE produto SET quantidade = quantidade - :quantidade WHERE preco = :preco');
					try {
						$ps->execute(array('quantidade' => $_POST['quantidade'], 'preco' => $_POST['preco']));
					} catch (PDOException $e) {
						echo 'Erro ao alterar os dados: ' . $e->getMessage();
					}
					header('Location: exercicio4.php');
				}
				
				
			?>
			
			<h1>Insira a quantidade maxima para a consulta</h1>
		
			<form id='f' name='f' action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post'>
				<label for='preco' >Quantidade:</label>
				<input type='text' id='quantidade' name='quantidade' /><br />
				<label for='preco' >Preço:</label>
				<input type='text' id='preco' name='preco' /><br />
				<input type='submit' id='alterar' name='alterar' value='Alterar' />
			</form>
		
	</body>
</html>