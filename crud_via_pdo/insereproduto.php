<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Pesquisa de contato por nome ou telefone</title>
	</head>
	<body>
			
			<?php
				header('Content-Type: text/html; charset=UTF-8');
				$pdo = null;
				try {
					$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
					$pdo = new PDO('mysql:dbname=empresa;hostname=localhost', 'root', 'jony1723', $options);
					$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				} catch(PDOException $e) {
					echo 'Falha ao conectar: ' . $e->getMessage();
				}
				
				if(isset($_POST['codigo']) and isset($_POST['nome']) and isset ($_POST['quantidade']) and isset ($_POST['preco'])) {
					if(isset($_POST['id'])) {
						$ps = $pdo->prepare('UPDATE produto SET codigo=?, nome=?, quantidade=?, preco=? WHERE id=?');
						try {
							$ps->execute(array($_POST['codigo'], $_POST['nome'], $_POST['quantidade'], $_POST['preco'], $_POST['id']));
						} catch (PDOException $e) {
							echo "<form id='f' name='f' action='exercicio4.php' method='get'>
								<input type='submit' id='voltar' name='voltar' value='Voltar' />
							</form>";
							echo 'Erro ao inserir: ' . $e->getMessage();
						}
					}
					else {
						$ps = $pdo->prepare('INSERT INTO produto(codigo, nome, quantidade, preco) values(?, ?, ?, ?)');
						try {
							$ok = $ps->execute(array($_POST['codigo'], $_POST['nome'], $_POST['quantidade'], $_POST['preco']));
						} catch (PDOException $e) {
							echo "<form id='f' name='f' action='exercicio4.php' method='get'>
								<input type='submit' id='voltar' name='voltar' value='Voltar' />
							</form>";
							echo 'Erro ao inserir: ' . $e->getMessage();
						}
					}
					
					header('Location: exercicio4.php');
				}
				
			?>
		
			<form id='f' name='f' action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post'>
				<?php 
					
					if(isset($_GET['id'])) {
						$ps = $pdo->prepare('SELECT * FROM produto WHERE id=?');
						try {
							$ok = $ps->execute(array($_GET['id']));
						} catch(PDOException $e) {
							echo 'Falha ao conectar: ' . $e->getMessage();
						}

						$p = $ps->fetchObject();
						echo "<input type='hidden' id='id' name='id' value='" . $p->id . " '/>" .
						"<label for='codigo' >Codigo:</label>" .
						"<input type='text' id='codigo' name='codigo' readonly='readonly' value='" . $p->codigo . "'/><br />" .
						"<label for='nome' >Nome:</label>" .
						"<input type='text' id='nome' name='nome' value='" . $p->nome . "'/><br />" .
						"<label for='quantidade' >Quantidade:</label>" .
						"<input type='text' id='quantidade' name='quantidade' value='" . $p->quantidade . "'/><br />" .
						"<label for='preco' >Preco:</label>" .
						"<input type='text' id='preco' name='preco' value='" . $p->preco . "'/><br />" .
						"<input type='submit' id='alterar' name='alterar' value='Alterar' />";
					}
					else {
						echo "<label for='codigo' >Codigo:</label>" .
						"<input type='text' id='codigo' name='codigo' /><br />" .
						"<label for='nome' >Nome:</label>" .
						"<input type='text' id='nome' name='nome' /><br />" .
						"<label for='quantidade' >Quantidade:</label>" .
						"<input type='text' id='quantidade' name='quantidade' /><br />" .
						"<label for='preco' >Preco:</label>" .
						"<input type='text' id='preco' name='preco' /><br />" .
						"<input type='submit' id='inserir' name='inserir' value='Inserir' />";
					}
				?>
			</form>
		
	</body>
</html>
