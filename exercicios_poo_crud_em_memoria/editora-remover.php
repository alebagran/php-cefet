<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('editora.php');
	require_once('colecaodeeditorasembdr.php');

	$pdo = null;
	try {
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$pdo = new PDO('mysql:dbname=magazine;hostname=localhost', 'root', 'jony1723', $options);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo 'Falha ao conectar: ' . $e->getMessage();
	}
	
	
	$editoras = new ColecaoDeEditorasEmBDR($pdo);

	
	
	if(isset($_GET['id'])) {
		$id = htmlspecialchars(trim($_GET['id']));
		try {
			$editoras->removerPeloId($id);
		} catch (ColecaoException $e) {
			echo 'Erro ao remover os dados' . $e->getMessage();
		}
		
		
		
	
		header('Location: editora-lista.php');
	}

?>
