<?php
	session_start();	

	
	
	if(!(isset($_SESSION['usuario']))) {
		
		unset($_SESSION['usuario']);
		
		header('Location: login.php');
	}
	
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Lista de Revistas</title>
	</head>
	<body>
	
		<table border ='1'>
	
		<?php 
			require_once 'colecaoderevistasembdr.php';
			require_once 'colecaodeeditorasembdr.php';
			require_once 'colecaodeeditoras.php';
			require_once 'revista.php';
			require_once 'pdoinstance.php';	
			
			header('Content-Type: text/html; charset=UTF-8');
			
			if(isset($_POST['usuario']) && isset($_POST['senha'])) {
				
				
				$usuario = htmlspecialchars(trim($_POST['usuario']));
				$senha = htmlspecialchars(trim($_POST['senha']));
				
				
				if(!($usuario == 'Ale' and $senha == '123'))
					header('Location: login.php');
				if(isset($_SESSION['usuario'])) {
					
					header('Location: login.php');	
				}
				
				
			}
			
			
			$pdoi = new PDOInstance();
			$pdo = $pdoi->getPDO();
			
			$editoraBDR = new ColecaoDeEditorasEmBDR($pdo);
			$revistaBDR = new ColecaoDeRevistasEmBDR($pdo, $editoraBDR);
			$colecao = $revistaBDR->todos();
		
			
	
			
		
			echo "<tr>" .
				 	"<td colspan='6' >Tamano da Lista: " . $revistaBDR->tamanho() . "</td>" .
				 "</tr>";
		?>
		
		
		
		
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Mes</th>
				<th>Ano</th>
				<th>Editora</th>
			</tr>
			
			
			
			
			
			
			
			<?php 
				$colecao = $revistaBDR->todos();
				foreach ($colecao as $e) {
					$revista = new Revista($e->id(), $e->nome(), $e->mes(), $e->ano(), $e->editora());
					echo "<tr>" .
							"<td>" . $e->id() . "</td>" .
							"<td>" . $e->nome() . "</td>" .
							"<td>" . $e->mes() . "</td>" .
							"<td>" . $e->ano() . "</td>" .
							"<td>" . $e->editora()->nome() . "</td>" .
							"<td><a href='revista-form.php?id=" . $e->id() . "'><img src='Pencil-icon.png'></a><a href='revista-remover.php?id=" . $e->id() . "'><img src='Actions-edit-delete-icon.png'></a></td>" .
							"</tr>";
				}
				
			?>
			
			
			
			
			
			
			
			
		</table>
		
		<br />
		
		<form id='f' name='f' action='revista-form.php' method='get'>
			<input type='submit' id='enviar' name='enviar' value='Cadastrar Revista' />
		</form>
		<form id='f' name='f' action='editora-lista.php' method='get'>
			<input type='submit' id='listar' name='listar' value='Lista de Editoras' />
		</form>
	</body>
</html>