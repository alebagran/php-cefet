<?php

session_start();

header('Content-Type: text/html; charset=UTF-8');

require_once 'revista.php';
require_once 'colecaodeeditorasembdr.php';
require_once 'colecaoderevistasembdr.php';
require_once 'pdoinstance.php';

$pdoi = new PDOInstance();
$pdo = $pdoi->getPDO();

if(!(isset($_SESSION['usuario'])) and !(isset($_SESSION['senha']))) {

	unset($_SESSION['usuario']);
	unset($_SESSION['senha']);

	header('Location: login.php');
}


	
	
	$editoraBDR = new ColecaoDeEditorasEmBDR($pdo);
	$revistaBDR = new ColecaoDeRevistasEmBDR($pdo, $editoraBDR);

	echo 'Foda-se';
	
	if(isset($_GET['id'])) {
		$id = htmlspecialchars(trim($_GET['id']));
		try {
			$revistaBDR->removerPeloId($id);
		} catch (ColecaoException $e) {
			echo 'Erro ao remover os dados' . $e->getMessage();
		}
		
		header('Location: revista-lista.php');
	}

?>