<?php
	session_start();
	

	require_once 'editora.php';
	require_once 'colecaodeeditorasembdr.php';
	require_once 'pdoinstance.php';
		
	
	header('Content-Type: text/html; charset=UTF-8');
	
	
	$pdoi = new PDOInstance();
	$pdo = $pdoi->getPDO();
	
	
	if(!(isset($_SESSION['usuario'])) and !(isset($_SESSION['senha']))) {
		
		unset($_SESSION['usuario']);
		unset($_SESSION['senha']);
		
		header('Location: login.php');
	}
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Cadastro de Editora</title>
	</head>
	<body>
		
	
		<h1>Cadastro de Editora</h1>
		
		<?php 
			
			$editora = new Editora();
			$editoras = new ColecaoDeEditorasEmBDR($pdo);
		
			
			
			if(isset($_GET['id'])) {
				$id = htmlspecialchars(trim($_GET['id']));
				try {
					$editora = $editoras->comId($id);
				} catch (ColecaoException $e) {
					echo 'Erro ao obter os dados: ' . $e->getMessage();
				}
				
			}
			else if(isset($_POST['enviar'])) {
				if(isset($_POST['nome']) and isset($_POST['id'])) {
					$nome = htmlspecialchars(trim($_POST['nome']));
					$id = htmlspecialchars(trim($_POST['id']));
					$editora = new Editora($id, $nome);
					if($id > 0) {
						try {
							$editoras->atualizar($editora);
						} catch (ColecaoException $e) {
							echo 'Erro ao atualizar os dados: ' . $e->getMessage();
						}	
					}
					else {
						try {
							$editoras->adicionar($editora);
						} catch (ColecaoException $e) {
							echo 'Erro ao adicionar os dados: ' .  $e->getMessage();
						}
					}
				
					header('Location: editora-lista.php');
				}
			}
		?>
		
		
		
		
	
		<form id="f" name="f" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
			<input type='hidden' id='id' name='id' value='<?php echo $editora->id(); ?>' /><br />
			<label for="nome">Nome: </label>
			<input type="text" id="nome" name="nome" value='<?php echo $editora->nome(); ?>' /><br />
			<input type="submit" id="enviar" name="enviar" value="Salvar"/><br />
		</form>
	
	
	</body>
</html>