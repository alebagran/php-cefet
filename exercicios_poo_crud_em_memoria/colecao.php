<?php
	interface Colecao {
		function adicionar($obj);
		function removerPeloId($id);
		function atualizar($obj);
		function comId($id);
		function todos();
		function tamanho();
	}
?>