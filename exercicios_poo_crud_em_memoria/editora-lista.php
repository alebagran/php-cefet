<?php
	session_start();
	

	require_once 'colecaodeeditorasembdr.php';
	require_once 'editora.php';
	require_once 'pdoinstance.php';
	
	header('Content-Type: text/html; charset=UTF-8');
	
	
	$pdoi = new PDOInstance();
	$pdo = $pdoi->getPDO();
	
	
	if(!(isset($_SESSION['usuario']))) {
		
		unset($_SESSION['usuario']);
		
		header('Location: login.php');
	}
	
?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head><title>Listagem de Editoras</title></head>
	<body>
	
		
	
		<table border ='1'>
		
		
		
		
		
		
			<?php 
				$editoraBDR = new ColecaoDeEditorasEmBDR($pdo);
				$colecao = $editoraBDR->todos();
			
				echo "<tr>" .
					 	"<td colspan='3' >Tamano da Lista: " . $editoraBDR->tamanho() . "</td>" .
					 "</tr>";
			?>
			
			
			
			
			
		
			<tr>
				<th>ID</th>
				<th>Nome</th>
			</tr>
			
			
			
			
			
			
			
			<?php
				
				
				
				foreach ($colecao as $e) {
					$editora = new Editora($e->id(), $e->nome());
					echo "<tr>" .
						 	"<td>" . $e->id() . "</td>" .
						 	"<td>" . $e->nome() . "</td>" .
						 	"<td><a href='editora-form.php?id=" . $e->id() . "'><img src='Pencil-icon.png'></a><a href='editora-remover.php?id=" . $e->id() . "'><img src='Actions-edit-delete-icon.png'></a></td>" .
						 "</tr>";
				}
				
			?>
				
				
				
				
				
				
		</table>
		
		<br />
		
		<form id='f' name='f' action='editora-form.php' method='get'>
			<input type='submit' id='enviar' name='enviar' value='Cadastrar Editora' />
		</form>
	
		<form id='f' name='f' action='revista-lista.php' method='get'>
			<input type='submit' id='listarRevista' name='listarRevista' value='Lista de Revistas' />
		</form>
	</body>
</html>
				
	
