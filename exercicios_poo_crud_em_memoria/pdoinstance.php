<?php
	class PDOInstance {
		private $pdo;
		private $options;
		
		public function __construct() {
			try {
				$this->options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
				$this->pdo = new PDO('mysql:dbname=magazine;hostname=localhost', 'root', 'jony1723', $this->options);
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				echo 'Falha ao conectar' . $e->getMessage();
			}
		}
		
		public function getPDO() {
			return $this->pdo;
		}
	}
?>