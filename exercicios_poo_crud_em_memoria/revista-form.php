<?php
	session_start();	

	
	
	if(!(isset($_SESSION['usuario']))) {
		
		unset($_SESSION['usuario']);
		
		header('Location: login.php');
	}
	
?>




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
		<title>Cadastro de Revista</title>
	</head>
	<body>
		
		<h1>Cadastro de Revista</h1>
		
		
		
		
		
		
		
		
		
		
		
	
		<?php 
				header('Content-Type: text/html; charset=UTF-8');
			
				require_once 'editora.php';
				require_once 'revista.php';
				require_once 'colecaoderevistasembdr.php';
				require_once 'colecaodeeditorasembdr.php';
				require_once 'pdoinstance.php';
				
				
				
				
				
				
				
				
				
				$pdoi = new PDOInstance();
				$pdo = $pdoi->getPDO();
				
				
				
				
				
				
				
				
				
				
				
				$revista = new Revista();
				$editoraBDR = new ColecaoDeEditorasEmBDR($pdo);
				$revistaBDR = new ColecaoDeRevistasEmBDR($pdo, $editoraBDR);
			
				
				
				
				
				
				
				
				
				
				
				
				
				if(isset($_GET['id'])) {
					$id = htmlspecialchars(trim($_GET['id']));
					try {
						$revista = $revistaBDR->comId($id);
						echo $revista->nome();
					} catch (ColecaoException $e) {
						echo 'Erro ao obter os dados: ' . $e->getMessage();
					}
				}
				else if(isset($_POST['enviar'])) {
					if(isset($_POST['id']) and isset($_POST['nome']) and isset($_POST['mes'])
and isset($_POST['ano']) and isset($_POST['editora'])) {
	
						$id = htmlspecialchars( trim( $_POST['id'] ) );
						$nome = htmlspecialchars( trim( $_POST['nome'] ) );
						$mes = htmlspecialchars( trim( $_POST['mes'] ) );
						$ano = htmlspecialchars( trim( $_POST['ano'] ) );
						$IdEditora = htmlspecialchars( trim( $_POST['editora']) );

						$editora = $editoraBDR->comId($IdEditora);
						$revista = new Revista($id, $nome, $mes, $ano, $editora);
						if($id > 0) {
							try {
								$revistaBDR->atualizar($revista);
							} catch (ColecaoException $e) {
								echo 'Erro ao atualizar os dados: ' . $e->getMessage();
							}
						}
						else {
							try {
								$revistaBDR->adicionar($revista);
							} catch (ColecaoException $e) {
								echo 'Erro ao adicionar os dados: ' .  $e->getMessage();
							}
						}
				
						header('Location: revista-lista.php');
					}
				}
			
			?>
			
			
			
			
			
			
			
			
			
			
			
			
			
		<form id="f" name="f" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
				<input type='hidden' id='id' name='id' value='<?php echo $revista->id(); ?>' /><br />
				<label for="nome">Nome: </label>
				<input type="text" id="nome" name="nome" value='<?php echo $revista->nome(); ?>' /><br />
				<label for="mes">Mes: </label>
				<input type="text" id="mes" name="mes" value='<?php echo $revista->mes(); ?>' /><br />
				<label for="ano">Ano: </label>
				<input type="text" id="ano" name="ano" value='<?php echo $revista->ano(); ?>' /><br />
				<label for="editora">Editora: </label>
				<select id="editora" name="editora">
				
				
				
				
				
				<?php 
					foreach ( $editoraBDR->todos() as $e )
					{
						if ( $e->nome() == $revistaBDR->comId( $id )->editora()->nome() )
							echo "<option selected='selected' value=" . $e->id() . " >" . $e->nome() . "</option>";
						else 
							echo "<option value=" . $e->id() . " >" . $e->nome() . "</option>";
					}
				?>
				
				
				
				
				
				
				</select>
				<input type="submit" id="enviar" name="enviar" value="Salvar"/><br />
			</form>
		
	</body>
</html>