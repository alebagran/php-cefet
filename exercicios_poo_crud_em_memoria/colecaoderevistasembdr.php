<?php
	require_once 'editora.php';
	require_once 'colecao.php';
	require_once 'colecaoemmemoria.php';
	require_once 'colecaoexception.php';
	require_once 'colecaoderevistas.php';

	class ColecaoDeRevistasEmBDR extends ColecaoEmMemoria implements ColecaoDeRevistas  {
		private $pdo;
		private $editoras;
	

		public function __construct($pdo, ColecaoDeEditoras $editoras) {
			$this->pdo = $pdo;
			$this->editoras = $editoras;
		}
		
		private function executar($sql, array $parametros = array(), $mensagemDeErro) {
			$ps = null;
			try {
				$ps = $this->pdo->prepare($sql);
				$ps->execute($parametros);
				return $ps;
			} catch(PDOException $e) {
				throw new ColecaoException($mensagemDeErro, 0, $e);
			}
		}
		
		public function adicionar( $obj ) {
			$this->executar( 'INSERT INTO revista(nome, mes, ano, fk_id_editora) VALUES(?, ?, ?, ?)',
					array( $obj->nome(), $obj->mes(), $obj->ano(), $obj->editora()->id() ), 'Erro ao adicionar.' );
			// Coloca o id gerado pelo banco de dados na revista
			$obj->setId($this->pdo->lastInsertId());
		}
		
		function removerPeloId($id) {
			$this->executar( 'DELETE FROM revista WHERE id_revista = ?',
					array($id), 'Erro ao remover.' );
		}
		
		public function atualizar($obj) {
			$this->executar( 'UPDATE revista SET nome = ?, mes = ?, ano = ?, fk_id_editora = ? WHERE id_revista = ?',
					array($obj->nome(), $obj->mes(), $obj->ano(), $obj->editora()->id(), $obj->id() ), 'Erro ao atualizar.' );
		}
		
		public function comId($id) {
			$ps = $this->executar( 'SELECT * FROM revista WHERE id_revista = ?',
					array($id), 'Erro ao obter a revista.' );
			
			$p = $ps->fetchObject();
			
			$editora = $this->editoras->comId($p->fk_id_editora);
			$revista = new Revista($p->id_revista, $p->nome, $p->mes, $p->ano, $editora);
			
			return $revista;
		}
		
		public function todos() {
			
			$revistas = array();
			
			$ps = $this->executar( 'SELECT * FROM revista',
					array(), 'Erro ao obter a lista.' );
			
			while($r = $ps->fetchObject()) {
				$editora = $this->editoras->comId($r->fk_id_editora);
				$revista = new Revista($r->id_revista, $r->nome, $r->mes, $r->ano, $editora);
				$revistas[] = $revista;
			}
			return $revistas;
		}
		
		public function tamanho() {
			$ps = $this->executar( 'SELECT COUNT(*) FROM revista',
					array(), 'Erro ao obter o tamanho.' );

			return $ps->fetchColumn();
		}
	}
?>