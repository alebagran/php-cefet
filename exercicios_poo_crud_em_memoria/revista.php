<?php
	class Revista {
		private $id;
		private $nome;
		private $mes;
		private $ano;
		private $editora;
		
		function __construct($id = '', $nome = '', $mes = '', $ano = '', $editora = '') {
			$this->id = $id;
			$this->nome = $nome;
			$this->mes = $mes;
			$this->ano = $ano;
			$this->editora = $editora;
		}
		
		function setId($id) {
			$this->id = $id;
		}
		
		function setNome($nome) {
			$this->nome = $nome;
		}
		
		function setMes($mes) {
			$this->mes = $mes;
		}
		
		function setAno($ano) {
			$this->ano = $ano;
		}
		
		function setEditora($editora) {
			$this->editora = $editora;
		}
		
		function id() {
			return $this->id;
		}
		
		function nome() {
			return $this->nome;
		}
		
		function mes() {
			return $this->mes;
		}
		
		function ano() {
			return $this->ano;
		}
		
		function editora() {
			return $this->editora;
		}
	}
?>