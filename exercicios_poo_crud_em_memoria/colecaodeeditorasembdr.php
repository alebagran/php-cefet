<?php
	require_once 'colecao.php';
	require_once 'colecaoemmemoria.php';
	require_once 'colecaodeeditoras.php';
	require_once 'colecaoexception.php';
	
	class ColecaoDeEditorasEmBDR extends ColecaoEmMemoria implements ColecaoDeEditoras {
		private $pdo;
		
		public function __construct($pdo) {
			$this->pdo = $pdo;
		}
		
		private function executar($sql, array $parametros = array(), $mensagemDeErro) {
			$ps = null;
			try {
				$ps = $this->pdo->prepare($sql);
				$ps->execute($parametros);
				return $ps;
			} catch(PDOException $e) {
				throw new ColecaoException($mensagemDeErro, 0, $e);
			}
		}
		
		public function adicionar ( $obj ) {
			$this->executar( 'INSERT INTO editora ( nome ) VALUES ( :nome )',
					array( 'nome' => $obj->nome() ), 'Erro ao adicionar.' );
		}
		
		public function removerPeloId($id) {
			$this->executar( 'DELETE FROM editora WHERE id_editora = ?',
					array ( $id ), 'Erro ao remover.' );
		}
		
		public function atualizar($obj) {
			$this->executar( 'UPDATE editora SET nome=? WHERE id_editora=?',
					array ( $obj->nome(), $obj->id() ), 'Erro ao atualizar.' );
		}
		
		public function comId($id) {
			$ps = $this->executar( 'SELECT * FROM editora WHERE id_editora=?',
					array ( $id ), 'Erro ao obter a editora.' );
			$e = $ps->fetchObject();
			return new Editora($e->id_editora, $e->nome);
		}
		
		public function todos() {
			$ps = $this->executar( 'SELECT * FROM editora', array (), 'Erro ao obter a lista.');
			$editoras = array();
			while($e = $ps->fetchObject()) {
				$editora = new Editora ( $e->id_editora, $e->nome );
				$editoras[] = $editora;
			}
			return $editoras;
		}
		
		// Melhorar - usar SELECT COUNT
		
		public function tamanho() {
			$ps = $this->executar( 'SELECT COUNT(*) FROM editora',
					array (), 'Erro ao obter o tamanho.' );
			
			
			/*
			 $contagem = $ps->fetchAll();
			 return count($contagem);
			 */
			return $ps->fetchColumn();
			
		}
		
		
		
	}
?>