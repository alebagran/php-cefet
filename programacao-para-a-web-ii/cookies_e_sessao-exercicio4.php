<?php
/*
 Sess�es:
 	
 	
 - Para identificar um cliente, indica-se guardar na sess�o o IP do cliente
 e compar�-lo a cada nova requisi��o, para certificar que o cliente ainda � o mesmo
 e evitar que algu�m se passe pelo cliente.
 
 - Fun��es:
 
 	session_start - cria/inicia uma sess�o
 	
 	session_id - retorna o ID da sess�o
 	
 	session_destroy - finaliza a sess�o
 	
 	session_cache_expire // Deve ser usada antes da session_start para determinar o tempo de expira��o
 	
 - Array global $_SESSION - permite acessar e manipular os dados de uma sess�o
 
 - Contador de acessos do cliente:
 
 session_start();
 <?php
 if ( isset( $_SESSION[ 'contador' ] ) ) {
 	$_SESSION[ 'contador' ] += 1;
 	}
 	else {
 		$_SESSION[ 'contador' ] = 1;
 	}
 	echo 'Acessos : ' . $_SESSION[ 'contador' ];
 	
 	- session_start deve ser sempre chamada antes de uma sa�da (echo, print, HTML, etc.)
 	e tamb�m deve ser chamada sempre antes de acessar a vari�vel $_SESSION, j� que esta vari�vel
 	manipula o conte�do da sess�o.
 	
 	- Remover dado da sess�o: unset( $_SESSION[ 'contador' ]); // Remove a chave contador do array
 	
 	- Limpar todos os dados - duas maneiras:
 	
 	session_unset();
 	
 	$_SESSION = array(); // Atribuir um array vazio � mesma.
 	
 	- Arquivo de sess�o - ao criar uma sess�o, estamos na verdade criando um arquivo
 	na pasta /tmp cujo nome possui prefixo "sess_" junto de uma string gerada aleatoriamente.
 	
 	Ex: sess_h1blbjqe6s4v8kuu6npdstak47
 	
 	Essa string ap�s o prefixo � a identifica��o da Sess�o, e pode ser obtida pela fun��o session_id
 	
 	- Fun��o session_id - permite tanto obter quanto definir a identifica��o da sess�o atual: 
 	
 	echo 'Atual: ', session_id();
 	$anterior = session_id( 'n0v4' );
 	echo '<br /> Atual: ', session_id();
 	echo '<br /> Anterior: ', $anterior;
 	
 	Se o servidor estiver configurado para n�o permitir o uso de sess�es, a session_id
 	ir� retornar uma string vazia.
 	
 	- O cliente recebe um cookie com o ID da sess�o, que varia dependendo da linguagem usada (EX: PHPSESSID).
 	
 	- console.log( document.cookie ); // visualiza o cookie no console Javascript do navegador.
 	
 	- Para obter os dados como ip e navegador do cliente:
 	
 	session_start(); // Cria/ inicia a sess�o
 	// Identifica o navegador e IP do cliente atual
 	$navegador = $_SERVER[ 'HTTP_USER_AGENT' ];
 	$ip = $_SERVER[ 'REMOTE_ADDR' ];
 	// Se a sess�o existe, ent�o h� gravado nela o navegador e o IP.
 	// Logo, comparamos com os identificadores
 	if( isset( $_SESSION[ 'navegador' ] )
 		&& isset( $_SESSION[ 'ip' ] )
 		&& ( $_SESSION[ 'navegador' ] !== $navegador
 		|| $_SESSION[ 'ip ] !== $ip ) ) {
 			die( 'Acesso negado.' );
 		}
 		
 	- IMPORTANTE:
 	
 	N�o guarde dados sens�veis em cookies
 	
 	Use sess�es sempre que poss�vel
 	
 	Proteja as sess�es contra roubo (XSS), identificando unicamente o cliente
 	
 	O comportamento dos navegadores em rela��o �s fun��es de sess�o podem variar.
 	Logo, teste seu c�digo em diferentes navegadores
 	
 	
 */
 ?>
 
 	<?php 
 		session_start();
 		
 		
 		
 		if( isset( $_POST['entrar'] ) ) {
 			if( isset( $_POST[ 'login' ] ) and isset( $_POST['senha'] ) ) {
 					
 						
 				
 				
 				$login = htmlspecialchars(trim($_POST[ 'login' ]));
 				$senha = htmlspecialchars(trim($_POST[ 'senha' ]));
 						
 				
 				if ( !( $login == 'Ale' and $senha == '12' ) ) {
 					die('Login ou senha inv�lidos!');
 				}
 						
 				
 				$navegador = $_SERVER[ 'HTTP_USER_AGENT' ];
 				$ip = $_SERVER[ 'REMOTE_ADDR' ];
 				
 				
 				
 				
 				if(!isset($_SESSION['usuario'])) {
 					$_SESSION[ 'navegador' ] = $navegador;
 					$_SESSION[ 'ip' ] = $ip;
 					$_SESSION[ 'usuario' ] = $login;
 				}
 		
 				
 				
 				if( isset( $_SESSION[ 'navegador' ] ) and isset( $_SESSION[ 'ip' ] ) ) {
 					echo 'navegador e ip setados!';
 					if ( $_SESSION[ 'navegador' ] !== $navegador || $_SESSION[ 'ip' ] !== $ip ) {
 						die( 'Acesso negado.' );
 					}
 				}
 		
 			
 			}
 		}
 		
 		echo '<br />';
 		
 		echo 'Conectado!';
 		
 		echo '<br />';
 		
 		
 		echo session_id();
 		echo '<br />';
		echo $_SESSION[ 'usuario' ];
		
		
 		
 		
		if(isset($_POST['logout'])) {
			session_destroy();
			header('Location: login.php');
		}
	?>
	
	

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	
	<meta http-equiv="Content-Type"
		content="text/html; charset=UTF-8" >
	
	
	
	<script>
		/*function obterCookie(nome) {
			setCookie
		}
		*/
	</script>
			
		
		<title>Cookies e Sessao - Exercicio 4</title>
	</head>
	<body>
	
		<form name='exit' id='exit' action='<?php echo $_SERVER[ 'PHP_SELF' ] ?>' method='post' >
			<input type='submit' name='logout' id='logout' value='Logout' />
		</form>

	</body>
</html>

		
		
		
		
