<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Cookies - Exerc�cio 2</title>
	</head>
	<body>
	
		<form id="f" name="f" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
			<label for="tarefa">Tarefa: </label>
			<input type="text" id="tarefa" name="tarefa" /><br />
			<input type="submit" id="adicionar" name="adicionar" value="adicionar"/><br />
			<input type="submit" id="apagar" name="apagar" value="apagar"/><br />
			<button type="button" id="mostrar" name="mostrar" onclick="mostraCookie()">mostrar</button>
		</form>
		
		<script type="text/javascript">
			function mostraCookie() {
				alert(document.cookie);
			}
		</script>
	
		<?php
			/*
			 Exerc�cio 3: Adicione um bot�o mostrar ao formul�rio do exerc�cio anterior,
			 que exiba o cookie via Javascript
			 */
			
			/*
			 Expira��o:
			 
			 $expiracao = time() + 3600; // Expira em uma hora
			 
			 $expiracao = time() + 3600 * 24 * 30 // Em 30 dias
			 
			 // Expira��o para �s 12:30:00 de 31/07/2050
			 $expiracao = mktime( hora, minuto, segundo, mes, dia, ano );
			 
			 setcookie( 'cep', '28613001', $expiracao );
			 
			 setcookie(
			 	'login',
			 	'juca@provedor.com',
			 	$expiracao,
			 	'/',
			 	'meusite.com',
			 	FALSE,
			 	FALSE,
			 	);
			 */
		
			 if(isset($_POST['apagar'])) {
				setcookie('task');
			}
			else if(isset($_POST['adicionar'])) {
				if(isset($_COOKIE['task'])) {
					$expiracao = time() + 60;
					$valor = $_COOKIE['task'] . $_POST['tarefa'];
					setcookie('task', $valor, $expiracao);
				}
				else {
					$expiracao = time() + 60;
					setcookie('task', $_POST['tarefa'], $expiracao);
				}
				
			}
		?>
		
	</body>
</html>