<?php
	require_once("colecao.php");
	require_once("colecaoexception.php");
	
	class ColecaoEmMemoria implements Colecao {
		private $itens = array();
		
		function adicionar($obj) {
			$this->itens[] = $obj;
		}
		
		public function removerPeloId($id) {
			if($id > count($this->itens) )
				throw new ColecaoException("Id inv�lido!");
			unset($this->itens[$id]);
		}
		
		function atualizar($obj) {
			if($obj->id() > count($this->itens))
				throw new ColecaoException("O objeto informado n�o existe!");
			$this->itens[$obj->id()] = $obj;
		}
		
		function comId($id) {
			if($id > count($this->itens) )
				throw new ColecaoException("Id inv�lido!");
			return $this->itens[$id];
		}
		
		function todos() {
			return $this->itens;
		}
		
		function tamanho() {
			return count($this->itens);
		}
	}
?>