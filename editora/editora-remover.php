<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('editora.php');

	$pdo = null;
	try {
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$pdo = new PDO('mysql:dbname=editora;hostname=localhost', 'root', '', $options);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo 'Falha ao conectar: ' . $e->getMessage();
	}
	
	if(isset($_GET['id'])) {
		$editora = new Editora($_GET['id']);
		$ps = $pdo->prepare('DELETE FROM colecaodeeditorasembdr WHERE id=?');
		try {
			$ps->execute(array($editora->id()));
		} catch (PDOException $e) {
			echo 'Erro ao alterar os dados: ' . $e->getMessage();
		}
		
		header('Location: editora-lista.php');
	}

?>
