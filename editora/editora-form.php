<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Cadastro de Editora</title>
	</head>
	<body>
		
	
		<h1>Cadastro de Editora</h1>
		
		<?php 
			header('Content-Type: text/html; charset=UTF-8');
			
			require_once('editora.php');
			
			$pdo = null;
			try {
				$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
				$pdo = new PDO('mysql:dbname=editora;hostname=localhost', 'root', '', $options);
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				echo 'Falha ao conectar: ' . $e->getMessage();
			}
		
			if(isset($_GET['id'])) {
				$ps = $pdo->prepare('SELECT * FROM colecaodeeditorasembdr WHERE id=?');
				try {
					$ps->execute(array($_GET['id']));
				} catch(PDOException $e) {
					echo 'Erro ao obter dados: ' . $e->getMessage(); 
				}
					
				$e = $ps->fetchObject();
				$editora = new Editora($e->id, $e->nome);
			}
			
			else if(isset($_GET['adicionar'])) {
				$editora = new Editora();
			}
		
			else if(isset($_POST['salvar'])) {
				if($_POST['id'] > 0) {
					$editora = new Editora($_POST['id'], $_POST['nome']);
					
					$ps = $pdo->prepare('UPDATE colecaodeeditorasembdr SET nome=? WHERE id=?');
					try {
						$ps->execute(array($editora->nome(), $editora->id()));
					} catch (PDOException $e) {
						echo 'Erro ao alterar os dados: ' . $e->getMessage();
					} 
				}
				else {
					$editora = new Editora($_POST['id'], $_POST['nome']);
					
					$ps = $pdo->prepare('INSERT INTO colecaodeeditorasembdr(nome) VALUES(?)');
					try {
						$ps->execute(array($editora->nome()));
					} catch (PDOException $e) {
						echo 'Erro ao inserir os dados: ' . $e->getMessage();
					}
				}
				
				header('Location: editora-lista.php');
			}
			
			else {
				$editora = new Editora();
			}
		?>
	
		<form id="f" name="f" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
			<input type='hidden' id='id' name='id' value='<?php echo $editora->id(); ?>' /><br />
			<label for="nome">Nome: </label>
			<input type="text" id="nome" name="nome" value='<?php echo $editora->nome(); ?>' /><br />
			<input type="submit" id="salvar" name="salvar" value="Salvar"/><br />
		</form>
	
	
	</body>
</html>