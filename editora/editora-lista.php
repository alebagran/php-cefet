<?php
header('Content-Type: text/html; charset=UTF-8');
$pdo = null;
try {
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
	$pdo = new PDO('mysql:dbname=editora;hostname=localhost', 'root', 'jony1723', $options);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	echo 'Falha ao conectar: ' . $e->getMessage();
}


	
?>
<html>
	<head><title>Listagem de Editoras</title></head>
	<body>
		<table border ='1'>
			<tr>
				<th>ID</th>
				<th>Nome</th>
			</tr>
				<?php
					header('Content-Type: text/html; charset=UTF-8');
					
					require_once('colecaodeeditorasemmemoria.php');
					require_once('editora.php');
					
					$pdo = null;
					try {
						$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
						$pdo = new PDO('mysql:dbname=editora;hostname=localhost', 'root', '', $options);
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					} catch(PDOException $e) {
						echo 'Falha ao conectar: ' . $e->getMessage();
					}
					
					$ps = $pdo->prepare('SELECT * FROM colecaodeeditorasembdr');
					try {
						$ps->execute();
					} catch (PDOException $e) {
						echo 'Erro ao realizar a consulta: ' . $e->getMessage(); 
					}
					
					$editoras = new ColecaoDeEditorasEmMemoria();
					
					while($e = $ps->fetchObject()) {
						$editora = new Editora($e->id, $e->nome);
						$editoras->adicionar($editora);
					}
					
					foreach($editoras->todos() as $ed){
						echo "<tr>" .
							"<td>" . $ed->id() . "</td>" .
							"<td>" . $ed->nome() . "</td>" .
							"<td><a href='editora-form.php?id=" . $ed->id() . "'><img src='Pencil-icon.png'></a><a href='editora-remover.php?id=" . $ed->id() . "'><img src='Actions-edit-delete-icon.png'></a></td>" .
							"</tr>";
					}
				?>
		</table>
		
		<form id='f' name='f' action='editora-form.php' method='get'>
			<input type='submit' id='adicionar' name='adicionar' value='Adicionar' />
		</form>
	</body>
</html>
				
	
