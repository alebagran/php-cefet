<?php
require_once 'colecaoexception.php';

interface Colecao {
	function adicionar($item);
	function remover($id);
	function atualizar($item);
	function comId($id);
	function todos();
	function tamanho();
}

?>