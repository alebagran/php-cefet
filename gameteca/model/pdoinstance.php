<?php
	class PDOInstance {
		private $pdo;
		
		function __construct() {
			$pdo = null;
			try {
				$option = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf-8');
				$pdo = new PDO('mysql:dbname=gameteca;hostname=localhost', 'root', '', $options);
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				echo 'Falha ao conectar: ' . $e->getMessage();
			}
		}
		
		function getPDO() {
			return $this->pdo;
		}
		
		
	}
?>