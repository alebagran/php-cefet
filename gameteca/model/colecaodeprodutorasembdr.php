<?php
class ColecaoDeProdutorasEmBDR {
	private $pdo = null;
	
	function __construct($pdo) {
		$this->pdo = $pdo;
	}
	
	private function executar( $sql, $mensagemDeErro, array $parametros = array() ) {
		try {
			$this->pdo->prepare( $sql );
			$this->execute( $parametros );
		} catch (PDOException $e) {
			throw new ColecaoException( $mensagemDeErro, 0, $e );
		}
	}
	
	function adicionar( &$item ) {
			$parametros( $item->getTitulo(), $item->getLancamento(), $item->getProdutora(), $item->getNumPlayers(), $item->getGenero() );
			$ps = $this->executar( 'INSERT INTO produtora( nome, endereco )
					VALUES( ?, ? )',
					'Erro ao adicionar os dados: ',
					$parametros );
			try {
				$item->setId($ps->lastInsertId());
			} catch (PDOException $e) {
				throw new ColecaoException( $mensagemDeErro, 0, $e );
			}
			
	}
	
	function remover( $id ) {
		$this->executar( 'DELETE from produtora WHERE produtora_id = ?',
				'Erro ao remover a produtora: ',
				$id );
	}
	
	function atualizar( $item ) {
		$this->executar( 'UPDATE produtora SET nome = ?, endereco = ?',
				'Erro ao atualizar os dados',
				array ( $item->getNome(), $item->getEndereco() ) );
	}
	
	function comId( $id ) {
		$ps = $this->executar( 'SELECT * FROM produtora WHERE produtora_id = ?', '', $id );
		$p = $ps->fetchObject();
		$produtora = new Produtora();
			
		}
	
	function todos() {
		$ps = $this->executar( 'SELECT * FROM produtora', 'Erro ao obter lista: ', $array() );
		$produtoras = array();
		while( $p = $ps->fetchObject() ) {
			$produtora = new Produtora( $p->nome, $p->endereco );
			$produtoras[] = $produtora;
		}
		return $produtoras;
	}
	
	function tamanho() {
		$ps = $this->executar( 'SELECT COUNT(produtora_id) FROM revista' );
			$resultado = $ps->fetchAll();
			return $resultado[0]['COUNT'];
	}
}
	
?>
	
		
