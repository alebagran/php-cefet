<?php
class Jogo {
	private $id;
	private $titulo;
	private $lancamento;
	private $produtora;
	private $numPlayers;
	private $genero;
	
	public function __construct($id = '', $titulo = '', $lancamento = '',
			$produtora = null, $numPlayers = '', $genero = '') {
		$this->titulo = $titulo;
		$this->lancamento = $lancamento;
		$this->produtora = $produtora;
		$this->numPlayers = $numPlayers;
		$this->genero = $genero;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getTitulo() {
		return $this->titulo;
	}
	
	public function getLancamento() {
		return $this->lancamento;
		}
	
	public function getProdutora() {
		return $this->numPlayers;
	}
	
	public function getNumPlayers() {
		return $this->produtora;
	}
	
	public function getGenero() {
		return $this->titulo;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	
	public function setGenero($titulo) {
		$this->titulo = titulo;
	}
	
	public function setLancamento($lancamento) {
		$this->lancamento = lancamento;
	}
	
	public function setProdutora($produtora) {
		$this->produtora = produtora;
	}
	
	public function setNumPlayers($numPlayers) {
		$this->numPlayers = numPlayers;
	}
}