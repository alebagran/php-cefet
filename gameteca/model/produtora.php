<?php
	class Produtora {
		private $id;
		private $nome;
		private $endereco;
		
		public function __construct($nome, $endereco) {
			$this->nome = $nome;
			$this->endereco = $endereco;
		}
		
		public function getId() {
			return $this->id;
		}
		
		public function getNome() {
			return $this->nome;
		}
		
		public function getEndereco() {
			return $this->endereco;
		}
		
		public function setId($id) {
			$this->id = $id;
		}
		
		
		public function setNome($nome) {
			$this->nome = $nome;
		}
		
		public function setEndereco($endereco) {
			$this->endereco = $endereco;
		}
		
		
	}
?>