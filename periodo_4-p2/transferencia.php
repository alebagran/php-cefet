<?php
class Transferencia {
	private $pdo;
	
	function __construct( $pdo ) {
		$this->pdo = $pdo;
	}
	
	private function executar( $sql, array $parametros = array() ) {
		$ps = $this->pdo->prepare( $sql );
		$p = $ps->execute( $parametros );
		return $ps;
	}
	
	function transferir($idOrigem, $cadastroOrigem, $valor, $idDestino, $cadastroDestino) {
		if ( $this->isPessoaJuridica ( $cadastroDestino ) ) {
			try {
				beginTransaction();
				
				
				$ps = $this->executar('SELECT * FROM conta WHERE id_conta = ?', array ( $idOrigem ));
				$p = $ps->fetchObject();
				
				if( $p->saldo < $valor ) {
					rollBack();
					throw new TransferenciaException('Saldo da conta de origem insuficiente');
				}
			
				$ps = $this->executar('UPDATE conta SET saldo = saldo - ? WHERE id_conta = ?', array ( $valor, $idOrigem ));
				
				$valorDoisPorCento = $valor * 0.02;
				$valorNovo = $valor - ($valor * 0.02); 
				
				$ps = $this->executar('UPDATE conta SET saldo = saldo + ? WHERE id_conta = ?', array ( $valorDoisPorCento, 0 ) );
				
				$ps = $this->executar('UPDATE conta SET saldo = saldo + ? WHERE id_conta = ?', array ( $valorNovo, $idDestino ) );
				
				
				commit();
					
					
			} catch (Exception $e) {
				rollBack();
				throw new TransferenciaException('Erro ao realizar transferencia');
			}
		}	
		else {
			try {
				beginTransaction();
					
					
				$ps = $this->executar('SELECT * FROM conta WHERE id_conta = ?', array ( $idOrigem ) );
				$p = $ps->fetchObject();
					
				if( $p->saldo < $valor ) {
					rollBack();
					throw new TransferenciaException('Saldo da conta de origem insuficiente');
				}
			
				$ps = $this->executar('UPDATE conta SET saldo = saldo - ? WHERE id_conta = ?', array ( $valor, $idOrigem ) );
								
				$ps = $this->executar('UPDATE conta SET saldo = saldo + ? WHERE id_conta = ?', array ( $valor, $idDestino ) );
				
				
				commit();
				
				
			} catch (Exception $e) {
				rollBack();
				throw new TransferenciaException('Erro ao realizar transferencia');
			}
		}
						
	}
}
