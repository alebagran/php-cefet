<?php
	session_start();
	
	$ip = $_SERVER['REMOTE_ADDR'];
	$navegador = $_SERVER['HTTP_USER_AGENT'];
	
	if($_SESSION['navegador'] != $navegador || $_SESSION['ip'] != $ip ) {
		
		$_SESSION = array();
		
		
		$_SESSION['navegador'] = $navegador;
		$_SESSION['ip'] = $ip;
	}
	 
	if(isset($_GET['operacao']) and isset($_GET['codigo_produto']) and isset($_GET['quantidade'])) {
		
		$operacao = htmlspecialchars( trim ( $_GET['operacao'] ) );
		$codigo_produto = htmlspecialchars( trim ( $_GET['codigo_produto'] ) );
		$quantidade = htmlspecialchars( trim ( $_GET['quantidade'] ) );
		
		
		
		
		if($operacao == '+') {
			if( ( !isset ( $_SESSION[$codigo_produto] ) ) ) {
				
				$_SESSION[$codigo_produto] = $quantidade;
				
			}
			else {
				$_SESSION[$codigo_produto] =  $_SESSION[$codigo_produto] + $quantidade;
			}
		}
		else {
			$_SESSION[$codigo_produto] = $_SESSION[$codigo_produto] - $quantidade;
			
			
			if($_SESSION[$codigo_produto] <= 0) {
				unset($_SESSION[$codigo_produto]);
			}
		}
		
	}
?>