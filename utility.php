// Useful stuff








// Sanitize all incoming data

function sanitize($input) {
    return htmlspecialchars(trim($input));
}

// Sanitize all the incoming data
$sanitized = array_map('sanitize', $_POST);










// Exemplo de ColecaoEmBDR

class ColecaoDeEditorasEmBDR extends ColecaoEmMemoria implements ColecaoDeEditoras {
		private $pdo;
		
		public function __construct($pdo) {
			$this->pdo = $pdo;
		}
		
		private function executar($sql, array $parametros = array(), $mensagemDeErro) {
			$ps = null;
			try {
				$ps = $this->pdo->prepare($sql);
				$ps->execute($parametros);
				return $ps;
			} catch(PDOException $e) {
				throw new ColecaoException($mensagemDeErro, 0, $e);
			}
		}
		
		public function adicionar ( $obj ) {
			$this->executar( 'INSERT INTO editora ( nome ) VALUES ( :nome )',
					array( 'nome' => $obj->nome() ), 'Erro ao adicionar.' );
		}
		
		public function removerPeloId($id) {
			$this->executar( 'DELETE FROM editora WHERE id_editora = ?',
					array ( $id ), 'Erro ao remover.' );
		}
		
		public function atualizar($obj) {
			$this->executar( 'UPDATE editora SET nome=? WHERE id_editora=?',
					array ( $obj->nome(), $obj->id() ), 'Erro ao atualizar.' );
		}
		
		public function comId($id) {
			$ps = $this->executar( 'SELECT * FROM editora WHERE id_editora=?',
					array ( $id ), 'Erro ao obter a editora.' );
			$e = $ps->fetchObject();
			return new Editora($e->id_editora, $e->nome);
		}
		
		public function todos() {
			$ps = $this->executar( 'SELECT * FROM editora', array (), 'Erro ao obter a lista.');
			$editoras = array();
			while($e = $ps->fetchObject()) {
				$editora = new Editora ( $e->id_editora, $e->nome );
				$editoras[] = $editora;
			}
			return $editoras;
		}
		
		// Melhorar - usar SELECT COUNT
		
		public function tamanho() {
			$ps = $this->executar( 'SELECT COUNT(*) FROM editora',
					array (), 'Erro ao obter o tamanho.' );
			
			/*
			 $contagem = $ps->fetchAll();
			 return count($contagem);
			 */
			return $ps->fetchColumn();
			
		}
	}







// Setting a shopping cart

// My way to do this is the following:

if (isset($_POST['buy'])) {
    $getData = $db->prepare('SELECT * FROM plans WHERE id=?');
    $getData->bind_param('i', $pl);
    if ($getData->execute()) {
        $res = $getData->get_result();
        if ($pn = $res->fetch_object()) {

            $item['name'] = $pn->plan_name;
            $item['price'] = $pn->price_dollar;

            $_SESSION['cart'][] = $item;
        }
    }
}
This way, even if the cart key isn't set, it will be set anyway. By the same time, it will permit you to have multiple items in your cart.

EDIT: The next thing to do, if you want to show your items is the following:

foreach ($_SESSION['cart'] as $k => $item) {
    echo "item $item->id $item->plan_name -- $item->price_dollar";
}







// Anoter take on shopping carts:

This is what you need to do:

Learn how to use session: http://www.w3schools.com/php/php_sessions.asp

Store the selected line items (object/array) in the session. For example: array(array('pAAA','2',550,"img/pAAA.jpg"),array('pAA1','2',550,"img/pAA1.jpg")); is stored in the named session called shopping_card.

At the product part page where you display the card, do the following:

 <?php if(isset($_SESSION['shopping_card'])): ?> //check if session is set ?>
     <div id="cart_list">
     <?php foreach($_SESSION['shopping_card'] as $product): ?>
               <!-- This should be display the cart list -->
     <?php end foreach;?>
    </div>
  <?php endif; ?>
Make sure that you have started the session. So everytime the user post something to the shopping card, the page refreshed.Hope it helps. I did not really check the syntax. My main objective is to give you the flow on how you should get started.

shareeditflag
answered Apr 15 '14 at 1:51

madi
1,88311732
  	
 		
Ok thanks for that I will do that. :) – Jerielle Apr 15 '14 at 1:54
add a comment