<?php


require_once 'marca.php';
require_once 'colecaodemarcasbdr.php';
require_once 'pdoinstance.php';

$pdoi = new PDOInstance();
$pdo = $pdoi->getInstance();

$marcaBDR = new ColecaoDeMarcasBDR($pdo);


if($_GET['id']) {
	$id = htmlspecialchars ( trim ( $_GET['id'] ) );
	$marcaBDR->remove( $id );
	header('Location: marca-lista.php');
}
else {
	die('Problema em marca-remover.');
}


?>