<?php
require_once 'marca.php';
require_once 'colecaoexception.php';

class ColecaoDeMarcasBDR {
	private $pdo;
	
	function __construct($pdo) {
		$this->pdo = $pdo;
	}
	
	private function executar( $sql, $errorMsg = '', array $parametros = array() ) {
		try {
			$ps = $this->pdo->prepare( $sql );
			$ps->execute( $parametros );
			return $ps;
		} catch (PDOException $e) {
			throw new ColecaoException( $errorMsg, 0, $e );
		}
	}
	
	function add( &$obj ) {
		if($obj->getId() > 0) {
			$ps = $this->executar( 'UPDATE marca SET nome = ? WHERE id_marca = ?',
					'Problema em ColecaoDeMarcasBDR - metodo add($obj) - UPDATE',
					array( $obj->getNome(), $obj->getId() ) );
		} 
		else {
			$ps = $this->executar('INSERT INTO marca(nome) values(?)',
					'Problema em ColecaoDeMarcasBDR - metodo add($obj) - ADD',
					array( $obj->getNome() ) );
			 $obj->setId ( $this->pdo->lastInsertId() );
		}
	}
	
	function remove($id) {
		$this->executar('DELETE FROM marca WHERE id_marca = ?',
				'Problema em ColecaoDeMarcasBDR - metodo remove($id)',
				array($id));
	}
	
	function todos() {
		$ps = $this->executar('SELECT * FROM marca',
				'Problema em ColecaoDeMarcasBDR - metodo todos()');
		$marcas = array();
		while( $p = $ps->fetchObject() ) {
			$marca = new Marca( $p->id_marca, $p->nome );
			$marcas[] = $marca;
		}
		return $marcas;
	}
	
	function comId($id) {
		$ps = $this->executar( 'SELECT * FROM marca WHERE id_marca = ?',
				'Problema em ColecaoDeMarcasBDR - metodo comId($id)',
				array( $id ) );
		$p = $ps->fetchObject();
		$marca = new Marca( $p->id_marca, $p->nome );
		return $marca;
		}
	
	function tamanho() {
		$ps = $this->executar('SELECT COUNT (*) FROM marca',
				'Problema em ColecaoDeMarcasBDR - metodo tamanho()');
		$linhas = $ps->fetchAll();
		return $linhas[0][0];
		//return $ps->fetchColumn;
	}
}
?>