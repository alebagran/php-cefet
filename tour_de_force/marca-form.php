<?php
require_once 'marca.php';
require_once 'colecaodemarcasbdr.php';
require_once 'pdoinstance.php';

$pdoi = new PDOInstance();
$pdo = $pdoi->getInstance();

$marcaBDR = new ColecaoDeMarcasBDR($pdo);


if( isset ( $_GET['id'] ) ) {
	$id = htmlspecialchars ( trim ( $_GET['id'] ) );
	$marca = $marcaBDR->comId($id);
}
else {
	$marca = new Marca();
}


if( isset ( $_POST['enviar'] ) ) {
	if( isset ( $_POST['id'] ) and isset ( $_POST['nome'] ) ) {
		
		$id = htmlspecialchars ( trim ( $_POST['id'] ) );
		$nome = htmlspecialchars ( trim ( $_POST['nome'] ) );
		
		$marca = new Marca($id, $nome);
		
		$marcaBDR->add ( $marca );
		
		header('Location: marca-lista.php');
	}			
}









?>

<html>


<head></head>


<body>
	
	<h1>Entre com o nome da marca: </h1>
	<form id='formAddMarca' name='formAddMarca'
	action='<?php echo $_SERVER['PHP_SELF'] ?>' method='post' >
	
		<input type='hidden' id='id' name='id' value='<?php echo $marca->getId(); ?>' /><br />
	
		<input type='text' id='nome' name='nome' value='<?php echo $marca->getNome(); ?>' /><br />
	
		<input type="submit" id='enviar' name='enviar' value='Adicionar' />
		
		
		
	</form>
	
	
	
</body>
</html>