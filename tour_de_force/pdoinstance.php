<?php
class PDOInstance {
	private $pdo;
	
	
	function __construct() {
		try {
			$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
			$this->pdo = new PDO('mysql:dbname=loja;hostname=localhost', 'root', 'jony1723', $options);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo 'Problema em PDOInstance() __construct()';
		}
	}
	
	function getInstance() {
		return $this->pdo;
	}
	
}
?>