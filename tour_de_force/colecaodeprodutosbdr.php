<?php
class ColecaoDeProdutosBDR {
	private $pdo;
	private $marcaBDR;
	
	function __construct($pdo, $marcaBDR) {
		$this->pdo = $pdo;
		$this->marcaBDR = $marcaBDR;
	}
	
	private function executar( $sql, $errorMsg = '', array $parametros = array() ) {
		try {
			$ps = $this->pdo->prepare( $sql );
			$ps->execute( $parametros );
			return $ps;
		} catch (PDOException $e) {
			throw new ColecaoException( $errorMsg, 0, $e );
		}
	}
	
	function add( &$obj ) {
		if($obj->getId() > 0) {
			$ps = $this->executar( 'UPDATE produto SET nome = ?, preco = ?, fk_id_marca = ? WHERE id_produto = ?',
					'Problema em ColecaoDeProdutosBDR - metodo add($obj) - UPDATE',
					array( $obj->getNome(), $obj->getPreco(), $obj->getMarca()->getId(), $obj->getId() ) );
		} 
		else {
			$ps = $this->executar('INSERT INTO produto(nome, preco, fk_id_marca) values(?, ?, ?)',
					'Problema em ColecaoDeProdutosBDR - metodo add($obj) - ADD',
					array( $obj->getNome(), $obj->getPreco(), $obj->getMarca()->getId() ) );
			return $ps->lastInsertId();
		}
	}
	
	function remove($id) {
		$this->executar('DELETE FROM produto WHERE id = ?',
				'Problema em ColecaoDeProdutosBDR - metodo remove($id)',
				array($id));
	}
	
	function todos() {
		$ps = $this->executar('SELECT * FROM produto',
				'Problema em ColecaoDeProdutosBDR - metodo todos()');
		$produtos = array();
		while( $p = $ps->fetchObject() ) {
			$produto = new Produto( $p->id, $p->nome, $p->preco, $marcaBDR->comId($p->fk_id_marca) );
			$produtos[] = $produto;
		}
		return $produtos;
	}
	
	function comId($id) {
		$ps = $this->executar( 'SELECT * FROM produto WHERE id_produto = ?',
				'Problema em ColecaoDeProdutosBDR - metodo comId($id)',
				array($obj->getId() ) );
		$p = $ps->fetchObject();
		$produto = new Produto( $p->id, $p->nome, $p->preco, $marcaBDR->comId( $p->fk_id_marca ) );
		return $produto;
		}
	
	function tamanho() {
		$ps = $this->executar('SELECT COUNT (*) FROM produto',
				'Problema em ColecaoDeProdutosBDR - metodo tamanho()');
		$linhas = $ps->fetchAll();
		return $linhas[0][0];
		//return $ps->fetchColumn;
	}
}
?>