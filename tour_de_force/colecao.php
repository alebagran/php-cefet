<?php
interface Colecao {
	function add($obj);
	function remove($id);
	function todos();
	function comId($id);
	function tamanho();
}